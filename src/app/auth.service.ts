import { reject } from 'q';
import { resolve } from 'path';

export class AuthService {
  loggegedIn = false;
  isAuthenticated() {
    const promise = new Promise(
      (resolve, reject) => {
        setTimeout(() => {
          resolve(this.loggegedIn);
        }, 800);
      }
    );
  }
  login() {
    this.loggegedIn = true;
  }
  logout() {
    this.loggegedIn = false;
  }
}
